/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.definition.provider.mock;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Spring module configuration.
 *
 * @author Rommel Medina
 *
 */
@Configuration
@ComponentScan(basePackages = "com.resolve.proportional.definition.provider.mock")
public class MockProportionalDefinitionProviderConfiguration {

}
