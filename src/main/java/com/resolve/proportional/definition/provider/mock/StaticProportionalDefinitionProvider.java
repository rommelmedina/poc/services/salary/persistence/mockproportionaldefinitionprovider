/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.definition.provider.mock;

import com.resolve.proportional.models.BonusPercentagesDefinition;
import com.resolve.proportional.models.CategoriesDefinition;
import com.resolve.proportional.models.CategoryBreakdown;
import com.resolve.proportional.models.ProportionalDefinition;
import com.resolve.salary.calculation.models.SalaryConstants;
import java.util.HashMap;

/**
 * First test cases for proportional definition calculation.
 *
 * @author Rommel Medina
 *
 */
public class StaticProportionalDefinitionProvider {
  /**
   * Definitions Lists.
   */
  private HashMap<String, ProportionalDefinition> defintions =
      new HashMap<String, ProportionalDefinition>();
  
  /**
   * Get proportional definition.
   *
   * @param id Id for recover definition.
   * @return proportionalDefinition.
   */
  public ProportionalDefinition get(String id) {
    id = id.trim().toLowerCase();
    ProportionalDefinition pd = this.defintions.get(id);
    if (pd == null) {
      switch (id) {
        case "resuelvefc":
          pd = this.getResuelveFc();
          break;
        case "rojo":
          pd = this.getRojo();
          break;
        case "azul":
          pd = this.getAzul();
          break;
        default:
          break;
      }
      if (pd != null) {
        this.defintions.put(id, pd);
      }
    }
    return pd;
  }
  
  /**
   * Retrieve proportional definition for ResuelveFC.
   *
   * @return
   */
  public ProportionalDefinition getResuelveFc() {
    ProportionalDefinition definition = new ProportionalDefinition();
    definition.setId("resuelvefc");
    BonusPercentagesDefinition bonusDefinition = new BonusPercentagesDefinition();
    bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_LEVEL, 0.50);
    bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_GROUP, 0.50);
    
    definition.setPercentages(bonusDefinition);
    
    CategoryBreakdown singlePlayerGoals = new CategoryBreakdown();
    singlePlayerGoals.setId(SalaryConstants.CATEGORY_NAME_LEVEL);
    singlePlayerGoals.addData("a", 5.0);
    singlePlayerGoals.addData("b", 10.0);
    singlePlayerGoals.addData("c", 15.0);
    singlePlayerGoals.addData("cuahu", 20.0);
    
    CategoryBreakdown grouoPlayerGoals = new CategoryBreakdown();
    grouoPlayerGoals.setId(SalaryConstants.CATEGORY_NAME_GROUP);
    grouoPlayerGoals.addData("resuelvefc", 50.0);
    
    CategoriesDefinition catDefinition = new CategoriesDefinition();
    catDefinition.addCategories(singlePlayerGoals.getId());
    catDefinition.addCategories(grouoPlayerGoals.getId());
    catDefinition.addBreakdowns(singlePlayerGoals.getId(), singlePlayerGoals);
    catDefinition.addBreakdowns(grouoPlayerGoals.getId(), grouoPlayerGoals);
    
    definition.setCategories(catDefinition);
    
    return definition;
  }
  
  /**
   * Retrieve proportional definition for Rojo.
   *
   * @return
   */
  public ProportionalDefinition getRojo() {
    ProportionalDefinition definition = new ProportionalDefinition();
    definition.setId("rojo");
    BonusPercentagesDefinition bonusDefinition = new BonusPercentagesDefinition();
    bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_LEVEL, 0.50);
    bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_GROUP, 0.50);
    
    definition.setPercentages(bonusDefinition);
    
    CategoryBreakdown singlePlayerGoals = new CategoryBreakdown();
    singlePlayerGoals.setId(SalaryConstants.CATEGORY_NAME_LEVEL);
    singlePlayerGoals.addData("a", 5.0);
    singlePlayerGoals.addData("b", 10.0);
    singlePlayerGoals.addData("c", 15.0);
    singlePlayerGoals.addData("cuahu", 20.0);
    
    CategoryBreakdown grouoPlayerGoals = new CategoryBreakdown();
    grouoPlayerGoals.setId(SalaryConstants.CATEGORY_NAME_GROUP);
    grouoPlayerGoals.addData("rojo", 19.0);
    
    CategoriesDefinition catDefinition = new CategoriesDefinition();
    catDefinition.addCategories(singlePlayerGoals.getId());
    catDefinition.addCategories(grouoPlayerGoals.getId());
    catDefinition.addBreakdowns(singlePlayerGoals.getId(), singlePlayerGoals);
    catDefinition.addBreakdowns(grouoPlayerGoals.getId(), grouoPlayerGoals);
    
    definition.setCategories(catDefinition);
    
    return definition;
  }
  
  /**
   * Retrieve proportional definition for Azul.
   *
   * @return
   */
  public ProportionalDefinition getAzul() {
    ProportionalDefinition definition = new ProportionalDefinition();
    definition.setId("azul");
    BonusPercentagesDefinition bonusDefinition = new BonusPercentagesDefinition();
    bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_LEVEL, 0.50);
    bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_GROUP, 0.50);
    
    definition.setPercentages(bonusDefinition);
    
    CategoryBreakdown singlePlayerGoals = new CategoryBreakdown();
    singlePlayerGoals.setId(SalaryConstants.CATEGORY_NAME_LEVEL);
    singlePlayerGoals.addData("a", 5.0);
    singlePlayerGoals.addData("b", 10.0);
    singlePlayerGoals.addData("c", 15.0);
    singlePlayerGoals.addData("cuahu", 20.0);
    
    CategoryBreakdown grouoPlayerGoals = new CategoryBreakdown();
    grouoPlayerGoals.setId(SalaryConstants.CATEGORY_NAME_GROUP);
    grouoPlayerGoals.addData("azul", 37.0);
    
    CategoriesDefinition catDefinition = new CategoriesDefinition();
    catDefinition.addCategories(singlePlayerGoals.getId());
    catDefinition.addCategories(grouoPlayerGoals.getId());
    catDefinition.addBreakdowns(singlePlayerGoals.getId(), singlePlayerGoals);
    catDefinition.addBreakdowns(grouoPlayerGoals.getId(), grouoPlayerGoals);
    
    definition.setCategories(catDefinition);
    
    return definition;
  }
}
